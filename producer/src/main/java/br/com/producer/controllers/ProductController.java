package br.com.producer.controllers;

import br.com.core.entities.Product;
import br.com.core.service.ProductService;
import br.com.producer.dtos.ProductDTO;
import br.com.producer.response.Response;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static br.com.core.constants.MessagingConfig.EXCHANGE;
import static br.com.core.constants.MessagingConfig.ROUTING_KEY;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Slf4j
@Getter
@RestController
@RequestMapping(value = "/product/api", produces = APPLICATION_JSON_VALUE)
public class ProductController {

    @Autowired
    private ProductService productService;

    @Autowired
    private RabbitTemplate template;

    @PostMapping("/create")
    @ResponseStatus(CREATED)
    public Response<Object> create(@Valid @RequestBody ProductDTO productDTO) {

        Product product = this.getProductService().save(productDTO.toProduct());

        getTemplate().convertAndSend(EXCHANGE, ROUTING_KEY, product);
        return Response.builder()
                .data(product)
                .message("Produto criado com sucesso.")
                .build();
    }

}
