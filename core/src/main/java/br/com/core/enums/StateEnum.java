package br.com.core.enums;

import lombok.Getter;

@Getter
public enum StateEnum {
    ENVIADO,
    ENTREGUE
}
