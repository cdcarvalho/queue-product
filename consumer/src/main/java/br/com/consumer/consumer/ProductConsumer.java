package br.com.consumer.consumer;

import br.com.core.entities.Product;
import br.com.core.service.ProductService;
import lombok.Getter;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

import static br.com.core.constants.MessagingConfig.QUEUE;
import static br.com.core.enums.StateEnum.ENTREGUE;

@Getter
@Component
public class ProductConsumer {

    @Autowired
    private ProductService productService;

    @RabbitListener(queues = QUEUE)
    public void consumeMessageFromQueue(Product product) {
          product.setState(ENTREGUE);
          product.setUpdated(new Date());
          this.getProductService().update(product);
          System.out.println("Produto Entregue: (ID) = " + product.getId());
    }
}
