package br.com.producer.response;

import lombok.*;
import org.springframework.data.domain.Page;

@Builder
@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
public class Response<T> {

	private T data;
	private String message;
	private Page<T> results;
}
