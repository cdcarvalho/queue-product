package br.com.core.service.impl;

import br.com.core.entities.Product;
import br.com.core.repositories.ProductRepository;
import br.com.core.service.ProductService;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Getter
@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Override
    public Product save(Product product) {
        return this.getProductRepository().save(product);
    }

    @Override
    public Product update(Product product) {
        return this.save(product);
    }
}
