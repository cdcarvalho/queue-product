CREATE TABLE product (
	id int NOT NULL AUTO_INCREMENT,
    description VARCHAR(100) NOT NULL,
    state VARCHAR(20) NOT NULL,
    created DATETIME NOT NULL,
    updated DATETIME,
    PRIMARY KEY (id)
);