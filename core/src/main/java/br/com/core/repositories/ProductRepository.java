package br.com.core.repositories;

import br.com.core.entities.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional(readOnly = true)
public interface ProductRepository extends CrudRepository<Product, Long> {

}
