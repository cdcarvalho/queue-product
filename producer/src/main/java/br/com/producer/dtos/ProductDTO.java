package br.com.producer.dtos;

import br.com.core.entities.Product;
import br.com.core.enums.StateEnum;
import lombok.*;

import java.util.Date;

import static br.com.core.enums.StateEnum.ENVIADO;


@Builder
@ToString
@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProductDTO {

    private Long id;
    private String description;
    private StateEnum state;
    private Date created;
    private Date updated;

    /**
     * Converte para Product com os dados de um productDTO.
     *
     * @return Product
     */
    public Product toProduct() {
        return Product.builder()
                .id(this.getId())
                .description(this.getDescription())
                .state(ENVIADO)
                .created(new Date())
                .build();
    }
}
