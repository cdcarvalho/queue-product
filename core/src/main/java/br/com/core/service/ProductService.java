package br.com.core.service;

import br.com.core.entities.Product;

public interface ProductService {

    /**
     * Save product
     * @param product
     * @return
     */
    Product save(Product product);

    /**
     * Update product
     * @param product
     * @return
     */
    Product update(Product product);

}

