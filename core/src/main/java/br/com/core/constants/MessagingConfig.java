package br.com.core.constants;

public class MessagingConfig {

    public static final String QUEUE = "product_queue";
    public static final String EXCHANGE = "product_exchange";
    public static final String ROUTING_KEY = "product_routingKey";
    public static final String HOST_NAME = "localhost";
    public static final String USERNAME = "admin";
    public static final String PASSWORD = "admin";

}
